﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CheckWords : MonoBehaviour
{
    [SerializeField] private Text userText; //text where user speech will be written
    [SerializeField] private string userString; //string where will contain userText
    [SerializeField] private string[] words; //list of words to be checked
    [SerializeField] private GameObject won; //won panel
    [SerializeField] private GameObject ExplainByYourself; //the current panel

    private void Update() {
        userString = userText.text; //put the userText value into a string
        CheckWord();
    }

    private void CheckWord() {
        if(userString.Contains(words[0])) {
            ExplainByYourself.SetActive(false);
            won.SetActive(true);
        }
        if (userString.Contains(words[1])) {
            ExplainByYourself.SetActive(false);
            won.SetActive(true);
        }
    }
}
